package main

import (
	"encoding/json"
	"fmt"
	"strings"
)

type z map[string]any

func main() {
	flatSchema := map[string]any{
		"1id": map[string]any{
			"type":   "string",
			"format": "uuid",
		},
		"2code": map[string]any{
			"type": "string",
		},
		"3name": map[string]any{
			"type": "string",
		},
		"4province.1id": map[string]any{
			"type":   "string",
			"format": "uuid",
		},
		"4province.2code": map[string]any{
			"type": "string",
		},
		"4province.3name": map[string]any{
			"type": "string",
		},
		"4province.4country.1id": map[string]any{
			"type":   "string",
			"format": "uuid",
		},
		"4province.4country.2code": map[string]any{
			"type": "string",
		},
		"4province.4country.3name": map[string]any{
			"type": "string",
		},
	}
	expected := z{
		"1id": z{
			"format": "uuid",
			"type":   "string",
		},
		"2code": z{
			"type": "string",
		},
		"3name": z{
			"type": "string",
		},
		"4province": z{
			"1type": "object",
			"2properties": z{
				"1id": z{
					"format": "uuid",
					"type":   "string",
				},
				"2code": z{
					"type": "string",
				},
				"3name": z{
					"type": "string",
				},
				"4country": z{
					"1type": "object",
					"2properties": z{
						"1id": z{
							"format": "uuid",
							"type":   "string",
						},
						"2code": z{
							"type": "string",
						},
						"3name": z{
							"type": "string",
						},
					},
				},
			},
		},
	}
	nestedSchema := nestedOpenAPISchema(flatSchema)
	b, _ := json.MarshalIndent(nestedSchema, "", "  ")
	fmt.Println(string(b))
	c, _ := json.MarshalIndent(expected, "", "  ")
	fmt.Println(string(c))
	fmt.Println(string(b) == string(c))
}

func nestedOpenAPISchema(flatSchema z) z {
	root := z{}
	for k, v := range flatSchema {
		next := root
		keys := strings.Split(k, ".")
		last := len(keys) - 1
		for i, k := range keys {
			next_, ok := next[k]
			if ok {
				next = next_.(z)
			} else {
				if i == last {
					next[k] = v
				} else {
					next_ = z{}
					next[k] = next_
					next = next_.(z)
				}
			}
			if i == last {
				break
			}
			next_, ok = next["2properties"]
			if ok {
				next = next_.(z)
			} else {
				next_ = z{}
				next["1type"] = "object"
				next["2properties"] = next_
				next = next_.(z)
			}
		}
	}
	return root
}
